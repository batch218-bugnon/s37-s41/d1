// dependencies
const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController.js");
const auth = require("../auth.js");

router.post("/checkEmail", (req, res) => {
	userController.checkEmailExist(req.body).then(resultFromController => res.send(resultFromController))
});

router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
})


router.post("/registerAdmin", (req, res) => {
	userController.registerAdmin(req.body).then(resultFromController => res.send(resultFromController))
})

router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});

//S38 Actvity - possible answers

// A
router.post("/details", (req, res) => {
	userController.getProfileA({userId : req.body.id}).then(resultFromController => res.send(resultFromController))
});

// B
router.get("/details/:id", (req, res) => {
	userController.getProfileB(req.params.id).then(result => res.send(result))
});


// S41 ACTIVITY
// Enroll a user
// Route to enroll user to a course
router.post("/enroll", auth.verify, (req, res) => {

	let data = {
		// User ID will be retrieved from the request header
		userId : auth.decode(req.headers.authorization).id,
		// Course ID will be retrieved from the request body
		courseId : req.body.courseId
	}

	userController.enroll(data).then(resultFromController => res.send(resultFromController));

});

// Allows us to export the "router" object that will be accessed in our "index.js" file
module.exports = router;



